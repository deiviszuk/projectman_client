import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import reduxThunk from 'redux-thunk';

import App from './components/app';
import RequireAuth from './components/auth/require_auth';
import Signin from './components/auth/signin';
import Welcome from './components/welcome';
import Signout from './components/auth/signout';
import Signup from './components/auth/signup';
import Projectman from './components/projectman';
import reducers from './reducers';
import { AUTH_USER } from './actions/types';

import ContactsIndex from './components/contacts_index';
import NotesIndex from './components/notes_index';
import ProjectsIndex from './components/projects_index';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
const token = localStorage.getItem('token');

if (token) {
  store.dispatch({ type: AUTH_USER});
}

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
      <IndexRoute component={Welcome} />
      <Route path="signin" component={Signin} />
      <Route path="signout" component={Signout} />
      <Route path="signup" component={Signup} />
      <Route path="projectman" component={RequireAuth(Projectman)} />
      <Route path="contacts" component={RequireAuth(ContactsIndex)} />
      <Route path="notes" component={RequireAuth(NotesIndex)} />
      <Route path="projects" component={RequireAuth(ProjectsIndex)} />
      </Route>
    </Router>
  </Provider>
  , document.querySelector('.app'));
