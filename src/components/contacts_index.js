import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import { fetchContacts, fetchContact, deleteContact} from "../actions";
import ReactTable from "react-table";
import Modal from 'react-modal';
import CreateContact from './Modals/CreateContact';
import EditContact from './Modals/EditContact';

const customStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0.5, 0.5, 0.5, 0.75)'
  },
  content: {
    border: '0',
    borderRadius: '4px',
    bottom: 'auto',
    minHeight: '10rem',
    left: '50%',
    padding: '2rem',
    position: 'fixed',
    right: 'auto',
    top: '50%',
    transform: 'translate(-50%,-50%)',
    minWidth: '20rem',
    width: '80%',
    maxWidth: '60rem'
  }
}

class ContactsIndex extends Component {
  constructor() {
    super();

    this.state = {
      CreateModalIsOpen: false,
      EditModalIsOpen: false,
      activeItemId: null,
      activeContact: {}}

    this.openModalCreate = this.openModalCreate.bind(this);
    this.closeModalCreate = this.closeModalCreate.bind(this);
    this.openModalEdit = this.openModalEdit.bind(this);
    this.closeModalEdit = this.closeModalEdit.bind(this);
    this.deleteContact = this.deleteContact.bind(this);
  }

  openModalCreate() {
    this.setState({ CreateModalIsOpen: true });
  }
  openModalEdit(item) {
    this.props.fetchContact(item).then((res) => {
      this.setState({
          EditModalIsOpen: true,
          activeContact: {
            id: res.data.id,
            name: res.data.name,
            surname: res.data.surname,
            email: res.data.email,
            phone: res.data.phone
          }
        })
    })
  }
  closeModalCreate() {
    this.setState({ CreateModalIsOpen: false });
    this.props.fetchContacts();
  }
  closeModalEdit() {
    this.setState({ EditModalIsOpen: false });
    this.props.fetchContacts();  
  }
  componentWillMount() {
    this.props.fetchContacts();
  }

  deleteContact(id) {
    return this.props.deleteContact(id);
  }
 

  render() {
    const initialValues = {
      initialValues: {
        name: this.state.activeContact.name,
        surname: this.state.activeContact.surname,
        email: this.state.activeContact.email,
        phone: this.state.activeContact.phone
      }
    }

    const tableData = this.props.contacts.contacts;
    return (
      <div>
        <br />
        <div className="card contacts mx-auto z-depth-2">
          <div className="card-body">
            <h2>Contacts&nbsp;&nbsp;</h2>
                <button className="btn btn-primary" onClick={this.openModalCreate}>New Contact</button>

                <Modal
                  isOpen={this.state.CreateModalIsOpen}
                  onRequestClose={this.closeModalCreate}
                  style={customStyles}
                  ariaHideApp={false}
                >
                  <CreateContact modalClose={this.closeModalCreate} />
                </Modal>

                <Modal
                  isOpen={this.state.EditModalIsOpen}
                  onRequestClose={this.closeModalEdit}
                  style={customStyles}
                  ariaHideApp={false}
                >
                  <EditContact 
                    {...initialValues}
                    modalClose={this.closeModalEdit}
                    itemId={this.state.activeItemId}
                    activeContact={this.state.activeContact}
                    deleteContact={this.deleteContact}
                    />
                </Modal>
            <ReactTable
              data={tableData}
              NoDataComponent={() => { return <span></span> }}
              filterable
              getTrProps={(state, rowInfo, column, instance) => ({
                onClick: e => this.openModalEdit(rowInfo.original.id)
              })}
              columns={[
                {
                  columns: [
                    {
                      Header: "First Name",
                      accessor: 'name',
                    },
                    {
                      Header: "Last Name",
                      accessor: 'surname',
                    },
                    {
                      Header: "Email",
                      accessor: 'email',
                    },
                    {
                      Header: "Phone",
                      accessor: 'phone',
                    }
                  ]
                }
              ]}
              defaultPageSize={15}
              className="-highlight"
            />
          </div>
        </div>
      </div >
    );
  }
}

function mapStateToProps(state) {
  return { contacts: state.contacts };
}

export default connect(mapStateToProps, { fetchContacts, fetchContact })(ContactsIndex);
