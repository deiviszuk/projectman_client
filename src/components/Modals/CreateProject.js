import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions/index';
import { browserHistory, Redirect } from 'react-router';


class CreateProject extends Component {

    handleFormSubmit(formProps) {
        this.props.createProject(formProps).then(() => {
            this.props.modalClose();
        });
    }

    renderAlert() {
        if (this.props.errorMessage) {
            return (
                <div className="alert alert-danger">
                    <strong>Oops!</strong> {this.props.errorMessage}
                </div>
            );
        }
    }

    render() {
        const { handleSubmit, fields: { name, deadline, description } } = this.props;


        return (

            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <br />
                <br />
                <br />
                <div>
                    <div>
                        <div className="card-body">
                            <div className="form-header deep-blue-gradient rounded">
                                <h3><i className="fa fa-crosshairs"></i>&nbsp;&nbsp;
                                New Project:</h3>
                            </div>

                            <fieldset className="md-form">
                                <i className="fa fa-user-circle prefix"></i>
                                <input {...name} type="text" id="materialFormRegisternameEx" className="form-control" />
                                <label htmlFor="materialFormRegisternameEx">Name</label>
                            </fieldset>
                            {name.touched && name.error && <div className="error">{name.error}</div>}
                            <br />
                            <fieldset className="md-form">
                                <i className="fa fa-exclamation-triangle prefix"></i>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Deadline:
                                <input {...deadline} type="date" id="materialFormRegisterEmailEx" className="form-control" />
                            </fieldset>
                            {deadline.touched && deadline.error && <div className="error">{dadline.error}</div>}
                            <br />
                            <fieldset className="md-form">
                                <i className="fa fa-pencil prefix"></i>
                                <textarea {...description} type="text" id="exampleFormControlTextarea3" className="md-textarea form-control" rows="4"></textarea>
                                <label htmlFor="exampleFormControlTextarea3">Description</label>
                            </fieldset>
                            {description.touched && description.error && <div className="error">{description.error}</div>}

                            {this.renderAlert()}
                            <div className="text-center mt-4">
                                <button action="submit" className="btn btn-primary">ADD</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        errorMessage: state.auth.error
    }
}

function validate(formProps) {
    const errors = {};

    if (!formProps.name) {
        errors.name = 'Please enter a name!'
    }
    return errors;
}

export default reduxForm({
    form: 'create_project',
    fields: ['name', 'deadline', 'description'],
    validate
}, mapStateToProps, actions)(CreateProject); 