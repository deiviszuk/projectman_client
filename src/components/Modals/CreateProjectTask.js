import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions/index';
import { browserHistory, Redirect } from 'react-router';


class CreateProjectTask extends Component {

    handleFormSubmit(formProps) {
        this.props.createProjectTask(this.props.projectId, formProps).then(() => {
            this.props.modalClose();
        });
    }

    renderAlert() {
        if (this.props.errorMessage) {
            return (
                <div className="alert alert-danger">
                    <strong>Oops!</strong> {this.props.errorMessage}
                </div>
            );
        }
    }

    render() {
        const { handleSubmit, fields: { description } } = this.props;


        return (

            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <br />
                <br />
                <br />
                <div>
                    <div>
                        <div className="card-body">
                            <div className="form-header deep-blue-gradient rounded">
                                <h3><i className="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;&nbsp;
                                New Task:</h3>
                            </div>
                            <br />
                            <fieldset className="md-form">
                                <i className="fa fa-pencil prefix"></i>
                                <input {...description} type="text" id="exampleFormControlTextarea3" className="md-textarea form-control"></input>
                                <label htmlFor="exampleFormControlTextarea3">Description</label>
                            </fieldset>
                            {description.touched && description.error && <div className="error">{description.error}</div>}

                            {this.renderAlert()}
                            <div className="text-center mt-4">
                                <button action="submit" className="btn btn-primary">ADD</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        errorMessage: state.auth.error
    }
}

function validate(formProps) {
    const errors = {};

    if (!formProps.description) {
        errors.description = 'Please enter a description!'
    }
    return errors;
}

export default reduxForm({
    form: 'create_project_task',
    fields: ['description'],
    validate
}, mapStateToProps, actions)(CreateProjectTask); 