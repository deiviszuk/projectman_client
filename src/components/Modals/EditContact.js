import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions/index';

class EditContact extends Component {

    handleFormSubmit(formProps) {
        this.props.updateContact(formProps, this.props.activeContact.id).then(() => {
            this.props.modalClose();
        });
    }

    renderAlert() {
        if (this.props.errorMessage) {
            return (
                <div className="alert alert-danger">
                    <strong>Oops!</strong> {this.props.errorMessage}
                </div>
            );
        }
    }


    render() {
        const { handleSubmit, fields: { name, surname, email, phone } } = this.props;
        const myInitialValues = {
            initialValues: {
              name: 'John Doe'
            }
          }

        return (
            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <br />
                <br />
                <br />
                <div>
                    <div>
                        <div className="card-body">
                            <div className="form-header deep-blue-gradient rounded">
                                <h3><i className="fa fa-address-book-o"></i> Edit Contact:</h3>
                            </div>

                            <fieldset className="md-form">
                                <i className="fa fa-user-circle prefix grey-text"></i>
                                <input {...name} type="text" id="materialFormRegisterNameEx" className="form-control" />
                            </fieldset>
                            {name.touched && name.error && <div className="error">{name.error}</div>}

                            <fieldset className="md-form">
                                <i className="fa fa-user-circle-o prefix grey-text"></i>
                                <input {...surname} type="text" id="materialFormRegisterSurnameEx" className="form-control" />
                            </fieldset>
                            {surname.touched && surname.error && <div className="error">{surname.error}</div>}

                            <fieldset className="md-form">
                                <i className="fa fa-envelope prefix grey-text"></i>
                                <input {...email} type="email" id="materialFormRegisterEmailEx" className="form-control" />
                            </fieldset>
                            {email.touched && email.error && <div className="error">{email.error}</div>}

                            <fieldset className="md-form">
                                <i className="fa fa-mobile prefix grey-text"></i>
                                <input {...phone} type="text" id="materialFormLoginPhoneEx" className="form-control" />
                            </fieldset>
                            {phone.touched && phone.error && <div className="error">{phone.error}</div>}

                            {this.renderAlert()}
                            <div className="text-center mt-4">
                                <button action="submit" className="btn btn-primary">Update</button>
                                <a onClick={() => {
                                    this.props.deleteContact(this.props.activeContact.id).then(() => {
                                        this.props.modalClose();
                                    });
                                }
                            } className="btn btn-danger">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errorMessage: state.auth.error
    }
}

function validate(formProps) {
    const errors = {};

    if (!formProps.name) {
        errors.name = 'Please enter a name!'
    }
    if (!formProps.surname) {
        errors.surname = 'Please enter a surname!'
    }
    if (!formProps.email) {
        errors.email = 'Please enter an email!'
    }
    if (!formProps.phone) {
        errors.phone = 'Please enter phone!'
    }

    return errors;
}

export default reduxForm({
    form: 'edit_contact',
    fields: ['name', 'surname', 'email', 'phone'],
    validate,
}, mapStateToProps, actions)(EditContact); 