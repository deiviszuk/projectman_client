import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

class Header extends Component {

    renderAuthLinks() {
        if (this.props.authenticated) {
            return <li className="nav-item">
                <Link to="/signout" className="nav-link">Sign Out</Link>
            </li>
        } else {
            return [
                <li className="nav-item" key={1}>
                    <Link to="/signin" className="nav-link">Sign In</Link>
                </li>,
                <li className="nav-item" key={2}>
                    <Link to="/signup" className="nav-link">Sign Up</Link>
                </li>
            ];
        }
    }

    renderNavLinks() {
        if (this.props.authenticated) {
            return [
                <li className="nav-item" key={4}>
                    <Link to="/projects" className="nav-link">Projects</Link>
                </li>,
                <li className="nav-item">
                    <Link to="/notes" className="nav-link" key={5}>Notes</Link>
                </li>,
                <li className="nav-item">
                    <Link to="/contacts" className="nav-link" key={6}>Contacts</Link>
                </li>,
            ]
        } else {
            return;
        }
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">

                <Link to="/projectman" className="navbar-brand"><i className="fa fa-calendar" aria-hidden="true"></i>
                </Link>

                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        {this.renderNavLinks()}
                    </ul>

                    <ul className="navbar-nav ml-auto">
                        {this.renderAuthLinks()}
                    </ul>
                </div>
            </nav>
        );
    }
}

function mapStateToProps(state) {
    return {
        authenticated: state.auth.authenticated
    };
}

export default connect(mapStateToProps)(Header);