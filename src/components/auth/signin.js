import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions';

class Signin extends Component {
    handleFormSubmit({ email, password }) {
        // Need to do something to log user in
        this.props.signinUser({ email, password });
    }

    renderAlert() {
        if (this.props.errorMessage) {
            return (
                <div className="alert alert-danger">
                    <strong>Oops!</strong> {this.props.errorMessage}
                </div>
            );
        }
    }

    render() {
        const { handleSubmit, fields: { email, password } } = this.props;

        return (

            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <br />
                <br />
                <br />
                <div className="row justify-content-md-center">
                    <div className="card col-md-4">
                        <div className="card-body">
                            <div className="form-header deep-blue-gradient rounded">
                                <h3><i className="fa fa-lock"></i> Login:</h3>
                            </div>

                            <fieldset className="md-form">
                                <i className="fa fa-envelope prefix grey-text"></i>
                                <input {...email} type="email" id="materialFormLoginEmailEx" className="form-control" />
                                <label htmlFor="materialFormLoginEmailEx">E-mail address</label>
                            </fieldset>

                            <fieldset className="md-form">
                                <i className="fa fa-lock prefix grey-text"></i>
                                <input {...password} type="password" id="materialFormLoginPasswordEx" className="form-control" />
                                <label htmlFor="materialFormLoginPasswordEx">Password</label>
                            </fieldset>

                            {this.renderAlert()}
                            <div className="text-center mt-4">
                                <button action="submit" className="btn btn-primary">Sign in</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        errorMessage: state.auth.error
    }
}

export default reduxForm({
    form: 'signin',
    fields: ['email', 'password']
}, mapStateToProps, actions)(Signin);
