import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import * as actions from '../../actions';

class Signup extends Component {


    handleFormSubmit(formProps) {
        // Need to do something to log user in
        this.props.signupUser(formProps);
        console.log(formProps);
    }
 
    renderAlert() {
        if (this.props.errorMessage) {
            return (
                <div className="alert alert-danger">
                    <strong>Oops!</strong> {this.props.errorMessage}
                </div>
            );
        }
    }

    render() {
        const { handleSubmit, fields: { name, email, password, passwordConfirm } } = this.props;


        return (

            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <br />
                <br />
                <br />
                <div className="row justify-content-md-center">
                    <div className="card col-md-4">
                        <div className="card-body">
                            <div className="form-header deep-blue-gradient rounded">
                                <h3><i className="fa fa-lock"></i> Sign Up:</h3>
                            </div>

                            <fieldset className="md-form">
                                <i className="fa fa-user prefix grey-text"></i>
                                <input {...name} type="text" id="materialFormRegisterNameEx" className="form-control" />
                                <label htmlFor="materialFormRegisterNameEx">Your name</label>
                            </fieldset>
                            {name.touched && name.error && <div className="error">{name.error}</div>}                            

                            <fieldset className="md-form">
                                <i className="fa fa-envelope prefix grey-text"></i>
                                <input {...email} type="email" id="materialFormRegisterEmailEx" className="form-control" />
                                <label htmlFor="materialFormRegisterEmailEx">E-mail address</label>
                            </fieldset>
                            {email.touched && email.error && <div className="error">{email.error}</div>}

                            <fieldset className="md-form">
                                <i className="fa fa-lock prefix grey-text"></i>
                                <input {...password} type="password" id="materialFormLoginPasswordEx" className="form-control" />
                                <label htmlFor="materialFormLoginPasswordEx">Password</label>
                            </fieldset>
                            {password.touched && password.error && <div className="error">{password.error}</div>}

                            <fieldset className="md-form">
                                <i className="fa fa-exclamation-triangle prefix grey-text"></i>
                                <input {...passwordConfirm} type="password" id="materialFormLoginPasswordExConf" className="form-control" />
                                <label htmlFor="materialFormLoginPasswordExConf">Confirm password</label>
                            </fieldset>
                            {passwordConfirm.touched && passwordConfirm.error && <div className="error">{passwordConfirm.error}</div>}                            

                            {this.renderAlert()}
                            <div className="text-center mt-4">
                                <button action="submit" className="btn btn-primary">Sign in</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        errorMessage: state.auth.error
    }
}

function validate(formProps) {
    const errors = {};

    if (formProps.password !== formProps.passwordConfirm) {
        errors.password = 'Passwords must match!';
    }
    if (!formProps.name) {
        errors.name = 'Please enter a name!'
    }
    if (!formProps.email) {
        errors.email = 'Please enter an email!'
    }
    if (!formProps.password) {
        errors.password = 'Please enter a password!'
    }
    if (!formProps.passwordConfirm) {
        errors.passwordConfirm = 'Please enter a password confirmation!'
    }

    return errors;
}

export default reduxForm({
    form: 'signup',
    fields: ['name', 'email', 'password', 'passwordConfirm'],
    validate
}, mapStateToProps, actions)(Signup); 