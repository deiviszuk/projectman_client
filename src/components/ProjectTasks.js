import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import { fetchProjectTasks, deleteProjectTask, finishProjectTask } from "../actions";
import CreateProjectTask from './Modals/CreateProjectTask';
import Modal from 'react-modal';

const customStyles = {
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0.5, 0.5, 0.5, 0.75)'
    },
    content: {
        border: '0',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '80%',
        maxWidth: '40rem'
    }
}


class ProjectTasks extends Component {
    constructor() {
        super();

        this.state = {
            CreateModalIsOpen: false
        }

        this.openModalCreate = this.openModalCreate.bind(this);
        this.closeModalCreate = this.closeModalCreate.bind(this);
        this.handleDeleteTask = this.handleDeleteTask.bind(this);
    }

    openModalCreate() {
        this.setState({ CreateModalIsOpen: true });
    }

    closeModalCreate() {
        this.setState({ CreateModalIsOpen: false });
        this.props.fetchProjectTasks(this.props.projectId);
    }

    handleDeleteTask(id) {
        this.props.deleteProjectTask(this.props.projectId, id).then(() => {
            this.props.fetchProjectTasks(this.props.projectId);
        })
    }

    handleFinishTask(id) {
        const value = { finished: true};
        this.props.finishProjectTask(this.props.projectId, id).then(() => {
           this.props.fetchProjectTasks(this.props.projectId);
        })
    }

    renderTasks() {
        return _.map(this.props.tasks.tasks, task => {
            return (
                <tr className={`projectRow ${task.finished ? "table-success" : null}`} key={task.id}>
                    <td className="align-middle"><b>{task.description}</b></td>
                    <td style={{ textAlign: "center" }} className="align-middle"><b><i className={`${task.finished ? "fa fa-check-square-o" : "fa fa-square-o"}`} aria-hidden="true"></i></b></td>
                    <td style={{ textAlign: "right" }}>
                        <div className="btn-group">
                            <a onClick={() => {
                                this.handleFinishTask(task.id)
                            }}>
                                <h1><i className={`${task.finished ? null : "fa fa-check text-success"}`} aria-hidden="true"></i></h1>
                            </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a onClick={() => {
                                this.handleDeleteTask(task.id)
                            }}>
                                <h1><i className="fa fa-trash-o text-danger" aria-hidden="true"></i></h1>
                            </a>
                        </div>
                    </td>
                </tr>
            );
        });
    }

    render() {
        return (
            <div>
                <table className="table table-hover table-striped white">
                    <thead className="mdb-color special-color-dark table-head">
                        <tr className="text-white">
                            <th style={{ width: 600 }}>&nbsp;&nbsp;&nbsp;Project Tasks</th>
                            <th style={{ width: 20 }}>Status</th>
                            <th style={{ width: 50, textAlign: "right" }}>
                                <button className="btn btn-success btn-md" onClick={this.openModalCreate}>
                                    <i className="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;new
              </button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTasks()}
                    </tbody>
                </table>

                <Modal
                    isOpen={this.state.CreateModalIsOpen}
                    onRequestClose={this.closeModalCreate}
                    style={customStyles}
                    ariaHideApp={false}
                >
                    <CreateProjectTask
                        projectId={this.props.projectId}
                        modalClose={this.closeModalCreate}
                    />
                </Modal>
            </div >

        );
    }
}

function mapStateToProps(state) {
    return { tasks: state.tasks };
}

export default connect(mapStateToProps, { fetchProjectTasks, deleteProjectTask, finishProjectTask })(ProjectTasks);