import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import { fetchNotes, deleteNote } from "../actions";
import CreateNote from './Modals/CreateNote';
import Modal from 'react-modal';

const customStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0.5, 0.5, 0.5, 0.75)'
  },
  content: {
    border: '0',
    borderRadius: '4px',
    bottom: 'auto',
    minHeight: '10rem',
    left: '50%',
    padding: '2rem',
    position: 'fixed',
    right: 'auto',
    top: '50%',
    transform: 'translate(-50%,-50%)',
    minWidth: '20rem',
    width: '80%',
    maxWidth: '40rem'
  }
}


class NotesIndex extends Component {

  constructor() {
    super();

    this.state = {
      CreateModalIsOpen: false
    }

    this.openModalCreate = this.openModalCreate.bind(this);
    this.closeModalCreate = this.closeModalCreate.bind(this);
    this.handleDeleteNote = this.handleDeleteNote.bind(this);
  }

  openModalCreate() {
    this.setState({ CreateModalIsOpen: true });
  }

  closeModalCreate() {
    this.setState({ CreateModalIsOpen: false });
    this.props.fetchNotes();
  }

  componentWillMount() {
    this.props.fetchNotes();
  }

  handleDeleteNote(id) {
    this.props.deleteNote(id).then(() => {
      this.props.fetchNotes();
    })
  }

  renderNotes() {
    return _.map(this.props.notes.notes, note => {
      return (
        <div className="notetext" key={note.id}>

          <button type="button" className="close delete-note-button mx-auto" aria-label="Close"
            onClick={() => {
              this.handleDeleteNote(note.id)}} >
            <span aria-hidden="true">&times;</span>
          </button>

          <b>{note.name}</b>
          <br />
          <p> </p>
          <br />
          <p>{note.description}</p>
        </div>
      );
    });
  }


  render() {
    return (
      <div>
        <button className="btn btn-success sticky-top" onClick={this.openModalCreate}>New Note</button>
        <div className="notebox">
          {this.renderNotes()}
        </div>

        <div className="col-md-3" >
          <Modal
            isOpen={this.state.CreateModalIsOpen}
            onRequestClose={this.closeModalCreate}
            style={customStyles}
            ariaHideApp={false}
          >
            <CreateNote modalClose={this.closeModalCreate} />
          </Modal>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { notes: state.notes };
}

export default connect(mapStateToProps, { fetchNotes, deleteNote})(NotesIndex);