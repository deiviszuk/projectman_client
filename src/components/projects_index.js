import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import { fetchProjectTasks, fetchProjects, deleteProject, fetchProject } from "../actions";
import CreateProject from './Modals/CreateProject';
import EditProject from './Modals/EditProject';
import ProjectTasks from '../components/ProjectTasks';
import Modal from 'react-modal';

const customStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0.5, 0.5, 0.5, 0.75)'
  },
  content: {
    border: '0',
    borderRadius: '4px',
    bottom: 'auto',
    minHeight: '10rem',
    left: '50%',
    padding: '2rem',
    position: 'fixed',
    right: 'auto',
    top: '50%',
    transform: 'translate(-50%,-50%)',
    minWidth: '20rem',
    width: '80%',
    maxWidth: '40rem'
  }
}

class ProjectsIndex extends Component {

  constructor() {
    super();

    this.state = {
      CreateModalIsOpen: false,
      EditModalIsOpen: false,
      activeItemId: null,
      activeProject: {},
      showProjectTasks: false,
    }

    this.openModalCreate = this.openModalCreate.bind(this);
    this.closeModalCreate = this.closeModalCreate.bind(this);
    this.handleDeleteProject = this.handleDeleteProject.bind(this);
    this.openModalEdit = this.openModalEdit.bind(this);
    this.closeModalEdit = this.closeModalEdit.bind(this);
    this.openTaskList = this.openTaskList.bind(this);
  }
  openModalEdit(id) {
    this.props.fetchProject(id).then((res) => {
      this.setState({
        EditModalIsOpen: true,
        activeProject: {
          id: res.data[0].id,
          name: res.data[0].name,
          deadline: res.data[0].deadline,
          description: res.data[0].description
        }
      })
    })
  }
  closeModalEdit() {
    this.setState({ EditModalIsOpen: false });
    this.props.fetchProjects();
  }
  openModalCreate() {
    this.setState({ CreateModalIsOpen: true });
  }
  closeModalCreate() {
    this.setState({ CreateModalIsOpen: false });
    this.props.fetchProjects();
  }

  componentWillMount() {
    this.props.fetchProjects();
  }

  handleDeleteProject(id) {
    return this.props.deleteProject(id).then(() => {
      this.closeModalEdit();
    })
  }

  openTaskList(id) {
    this.props.fetchProjectTasks(id);

    this.setState({
      activeItemId: id,
      showProjectTasks: true
    });
  }

  renderProjects() {
    return _.map(this.props.projects.projects, project => {
      return (
        <tr className={`${project.id == this.state.activeItemId ? "table-danger" : ""}`} height={30} key={project.id} onClick={e => {
          this.openTaskList(project.id)
        }}>
          <td className="align-middle"><b>{project.name}</b></td>
          <td style={{ width: 100, textAlign: "center" }} className="align-middle"><b>{project.deadline}</b></td>
          <td className="align-middle"><b>{project.description}</b></td>
          <td style={{ width: 120, align: "right" }} className="align-middle">
            <button style={{ width: 100 }} className="btn btn-warning btn-md" onClick={() => this.openModalEdit(project.id)}>
              <i className="fa fa-wrench" aria-hidden="true"></i>&nbsp;&nbsp;Edit
            </button>
          </td>
        </tr>
      );
    });
  }


  render() {
    const initialValues = {
      initialValues: {
        id: this.state.activeProject.id,
        name: this.state.activeProject.name,
        deadline: this.state.activeProject.deadline,
        description: this.state.activeProject.description
      }
    }

    return (
      <div className="projects-container pt-3">
        <div className={`projects ${this.state.showProjectTasks ? "narrow" : ""}`}>
          <table className="table table-hover table-striped white">
            <thead className="mdb-color special-color-dark table-head">
              <tr className="text-white" height="30">
                <th style={{ width: 300 }}>&nbsp;&nbsp;&nbsp;Project Name</th>
                <th style={{ width: 50, textAlign: "center" }}>Deadline</th>
                <th>Description</th>
                <th style={{ textAlign: "right" }}>
                  <button style={{ width: 100 }} className="btn btn-success btn-md mr-3" onClick={this.openModalCreate}>
                    <i className="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;new
                </button>
                </th>
              </tr>
            </thead>
            <tbody>
              {this.renderProjects()}
            </tbody>
          </table>

          <Modal
            isOpen={this.state.CreateModalIsOpen}
            onRequestClose={this.closeModalCreate}
            style={customStyles}
            ariaHideApp={false}
          >
            <CreateProject modalClose={this.closeModalCreate} />
          </Modal>

          <Modal
            isOpen={this.state.EditModalIsOpen}
            onRequestClose={this.closeModalEdit}
            style={customStyles}
            ariaHideApp={false}
          >
            <EditProject
              {...initialValues}
              modalClose={this.closeModalEdit}
              itemId={this.state.activeItemId}
              activeProject={this.state.activeProject}
              projectDelete={this.handleDeleteProject}
            />
          </Modal>
        </div>
        <div className={`tasks ${this.state.showProjectTasks ? "visible" : ""}`}>
          {this.state.showProjectTasks ? <ProjectTasks projectId={this.state.activeItemId} /> : null}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { projects: state.projects };
}

export default connect(mapStateToProps, { fetchProjectTasks, fetchProjects, fetchProject, deleteProject })(ProjectsIndex);