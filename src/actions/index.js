import axios from 'axios';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import {
    AUTH_USER,
    DEAUTH_USER,
    AUTH_ERROR,
    CLEAR_ERROR,
    FETCH_CONTACTS,
    FETCH_CONTACT,
    CREATE_CONTACT,
    DELETE_CONTACT,
    UPDATE_CONTACT,
    FETCH_NOTES,
    CREATE_NOTE,
    DELETE_NOTE,
    FETCH_PROJECTS,
    FETCH_PROJECT,
    CREATE_PROJECT,
    UPDATE_PROJECT,
    DELETE_PROJECT,
    FETCH_PROJECT_TASKS,
    DELETE_PROJECT_TASK,
    UPDATE_PROJECT_TASK,
    CREATE_PROJECT_TASK,
    FINISH_PROJECT_TASK
} from './types';

const ROOT_URL = 'http://projectman.test/api';

/////////////////////////////////////////////////////////////////////////////////

export function signinUser({ email, password }) {
    return function (dispatch) {
        axios.post(`${ROOT_URL}/login`, { email, password })
            .then(response => {
                
                dispatch({ type: AUTH_USER });

                localStorage.setItem('token', response.data.data.token)
                dispatch({ type: CLEAR_ERROR });
                browserHistory.push('/projectman');
            })
            .catch(() => {
                dispatch(authError('Bad Login Info'));
            })
    }
}

export function signupUser({ name, email, password }) {
    return function (dispatch) {
        axios.post(`${ROOT_URL}/register`, { name, email, password })
            .then(response => {
                if (response.data.success) {
                    dispatch({ type: CLEAR_ERROR });
                    browserHistory.push('/signin');
                }
                if (!response.data.success) {
                    dispatch(authError(response.data.error.email.toString()));
                }
            });
    }
}

export function signoutUser() {
    browserHistory.push('/');
    localStorage.removeItem('token');
    return { type: DEAUTH_USER }
}

export function authError(error) {
    return {
        type: AUTH_ERROR,
        payload: error
    };
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function fetchContacts() {
    return function (dispatch) {
        axios.get(`${ROOT_URL}/contacts`, {
            headers: { authorization: 'bearer ' + localStorage.getItem('token') }
        }).then(request => {
            dispatch({
                type: FETCH_CONTACTS,
                payload: request
            })
        }).catch(e => {
            localStorage.removeItem('token');
            browserHistory.push('/signin');
        })
    }
}

export function fetchContact(id) {
    return function (dispatch) {
        return axios.get(`${ROOT_URL}/contacts/${id}`, {
            headers: { authorization: 'bearer ' + localStorage.getItem('token') }
        }).catch(e => {
            localStorage.removeItem('token');
            browserHistory.push('/signin');
        })
    }
}

export function createContact(values) {
    return function (dispatch) {
        return axios.post(`${ROOT_URL}/contacts`, values,
            { headers: { authorization: 'bearer ' + localStorage.getItem('token') } })
            .then(request => {
                dispatch({
                    type: CREATE_CONTACT,
                    payload: request
                });
            });
    }
}

export function updateContact(values, id) {
    return function (dispatch) {
        return axios.put(`${ROOT_URL}/contacts/${id}`, values,
            { headers: { authorization: 'bearer ' + localStorage.getItem('token') } })
    }
}

export function deleteContact(id) {
    return function (dispatch) {
        return axios.delete(`${ROOT_URL}/contacts/${id}`, {
            headers: { authorization: 'bearer ' + localStorage.getItem('token') }
        }).catch(e => {
            localStorage.removeItem('token');
            browserHistory.push('/signin');
        })
    }
}
////////////// Notes ////////////////////

export function fetchNotes() {
    return function (dispatch) {
        axios.get(`${ROOT_URL}/notes`, {
            headers: { authorization: 'bearer ' + localStorage.getItem('token') }
        }).then(request => {
            dispatch({
                type: FETCH_NOTES,
                payload: request
            })
        }).catch(e => {
            localStorage.removeItem('token');
            browserHistory.push('/signin');
        })
    }
}

export function createNote(values) {
    return function (dispatch) {
        return axios.post(`${ROOT_URL}/notes`, values,
            { headers: { authorization: 'bearer ' + localStorage.getItem('token') } })
            .then(request => {
                dispatch({
                    type: CREATE_NOTE,
                    payload: request
                });
            });
    }
}

export function deleteNote(id) {
    return function (dispatch) {
        return axios.delete(`${ROOT_URL}/notes/${id}`, {
            headers: { authorization: 'bearer ' + localStorage.getItem('token') }
        }).catch(e => {
            localStorage.removeItem('token');
            browserHistory.push('/signin');
        })
    }
}

////////////// Projects ////////////////////

export function fetchProjects() {
    return function (dispatch) {
        axios.get(`${ROOT_URL}/projects`, {
            headers: { authorization: 'bearer ' + localStorage.getItem('token') }
        }).then(request => {
            dispatch({
                type: FETCH_PROJECTS,
                payload: request
            })
        }).catch(e => {
            localStorage.removeItem('token');
            browserHistory.push('/signin');
        })
    }
}

export function fetchProject(id) {
    return function (dispatch) {
        return axios.get(`${ROOT_URL}/projects/${id}`, {
            headers: { authorization: 'bearer ' + localStorage.getItem('token') }
        })
        // .catch(e=>{
        //     localStorage.removeItem('token');
        //     browserHistory.push('/signin');
        // })
    }
}

export function createProject(values) {
    return function (dispatch) {
        return axios.post(`${ROOT_URL}/projects`, values,
            { headers: { authorization: 'bearer ' + localStorage.getItem('token') } })
            .then(request => {
                dispatch({
                    type: CREATE_PROJECT,
                    payload: request
                });
            });
    }
}

export function updateProject(values, id) {
    return function (dispatch) {
        return axios.put(`${ROOT_URL}/projects/${id}`, values,
            { headers: { authorization: 'bearer ' + localStorage.getItem('token') } })
    }
}

export function deleteProject(id) {
    return function (dispatch) {
        return axios.delete(`${ROOT_URL}/projects/${id}`, {
            headers: { authorization: 'bearer ' + localStorage.getItem('token') }
        });
    }
}

///////////////////// TASKS ///////////////////////
export function fetchProjectTasks(id) {
    return function (dispatch) {
        axios.get(`${ROOT_URL}/projects/${id}/all-tasks`, {
            headers: { authorization: 'bearer ' + localStorage.getItem('token') }
        }).then(request => {
            dispatch({
                type: FETCH_PROJECT_TASKS,
                payload: request
            })
        })
    }
}

export function createProjectTask(id, values) {
    return function (dispatch) {
        return axios.post(`${ROOT_URL}/projects/${id}/add-task`, values,
            { headers: { authorization: 'bearer ' + localStorage.getItem('token') } })
            .then(request => {
                dispatch({
                    type: CREATE_PROJECT_TASK,
                    payload: request
                });
            });
    }
}

export function deleteProjectTask(id, taskId) {
    return function (dispatch) {
        return axios.delete(`${ROOT_URL}/projects/${id}/delete-task/${taskId}`, 
            { headers: { authorization: 'bearer ' + localStorage.getItem('token') } })
            .then(request => {
                dispatch({
                    type: DELETE_PROJECT_TASK,
                    payload: request
                });
            });
    }
}

export function finishProjectTask(id, taskId) {
    return function (dispatch) {
        return axios.put(`${ROOT_URL}/projects/${id}/finish-task/${taskId}`, {finished:true}, 
            { headers: { authorization: 'bearer ' + localStorage.getItem('token') } })
            .then(request => {
                dispatch({
                    type: FINISH_PROJECT_TASK,
                    payload: request
                });
            });
    }
}