import { FETCH_PROJECTS, CREATE_PROJECT, DELETE_PROJECT, FETCH_PROJECT, UPDATE_PROJECT} from "../actions/types";

export default function (state = [], action) {
  switch (action.type) {
    case FETCH_PROJECTS:
      return { ...state, projects: action.payload.data };
    case FETCH_PROJECT:
      return action.payload.data;
    case UPDATE_PROJECT:
      return { ...state, projects: action.payload.data };
    case DELETE_PROJECT:
      return _.omit(state, action.payload);
    default:
      return state;
  }
}
