import { FETCH_NOTES, CREATE_NOTE, DELETE_NOTE } from "../actions/types";

export default function (state = [], action) {
  switch (action.type) {
    case FETCH_NOTES:
      return { ...state, notes: action.payload.data };
    default:
      return state;
  }
}
