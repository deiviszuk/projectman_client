import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import authReducer from  './auth_reducer';
import ContactsReducer from  './contacts_reducer';
import NotesReducer from  './notes_reducer';
import ProjectsReducer from  './projects_reducer';
import ProjectTasksReducer from  './project_tasks_reducer';

const rootReducer = combineReducers({
  form,
  auth: authReducer,
  contacts: ContactsReducer,
  notes: NotesReducer,
  projects: ProjectsReducer,
  tasks: ProjectTasksReducer
});

export default rootReducer;
