import _ from "lodash";
import { FETCH_CONTACTS, FETCH_CONTACT, DELETE_CONTACT, UPDATE_CONTACT } from "../actions/types";

export default function (state = [], action) {
  switch (action.type) {
    case DELETE_CONTACT:
      return _.omit(state, action.payload);
    case FETCH_CONTACT:
      return action.payload.data;
    case FETCH_CONTACTS:
      return { ...state, contacts: action.payload.data };
    case UPDATE_CONTACT:
      return { ...state, contacts: action.payload.data };
    default:
      return state;
  }
}
