import { FETCH_PROJECT_TASKS, DELETE_PROJECT_TASK, UPDATE_PROJECT_TASK, FINISH_PROJECT_TASK} from "../actions/types";

export default function (state = [], action) {
  switch (action.type) {
    case FETCH_PROJECT_TASKS:
      return { ...state, tasks: action.payload.data };
    case UPDATE_PROJECT_TASK:
      return { ...state, tasks: action.payload.data };
    case FINISH_PROJECT_TASK:
      return { tasks: action.payload.data };
    case DELETE_PROJECT_TASK:
      return _.omit(state, action.payload);
    default:
      return state;
  }
}
